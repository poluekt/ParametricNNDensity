import math, sys

sys.path.append("../")

from TensorFlowAnalysis import *

md = 6.270
ma = 5.280
mb = 0.139
mc = 0.498

etamin = 2. 
etamax = 5.
meanpt = 5.

observablesPhaseSpace = DalitzPhaseSpace( ma, mb, mc, md )

observablesTitles = [ "m^{2}(B#pi) (GeV^{2})", "m^{2}(K#pi) (GeV^{2})" ]

observablesData = [ "m2bhm", "m2hh" ]

observablesToys = [ "m2ab", "m2bc" ]

parametersList = [
    ("bptcut",  "Threshold p_{T}(B) (GeV)",   (0., 20.), -20.), 
    ("kptcut",  "Threshold p_{T}(K) (GeV)",   (0.3, 4.), -2.), 
    ("piptcut", "Threshold p_{T}(#pi) (GeV)", (0.3, 3.), -2.), 
    ("bkptcut", "Threshold p_{T}(BK) (GeV)",  (0., 30.), None), 
    ("bpiptcut","Threshold p_{T}(B#pi) (GeV)",(0., 30.), None), 
    ("kpiptcut","Threshold p_{T}(K#pi) (GeV)",(0.,  5.), None), 
    ("bcptcut", "Threshold p_{T}(Bc) (GeV)",  (0., 30.), 5. ), 
]

bounds = { i[0] : (i[2], i[3]) for i in parametersList }

def UniformRandom(rnd, x1, x2) : 
  """
    Uniform random numbers from x1 to x2
  """
#  print x1, x2
  return x1 + rnd*(x2-x1)

def GenerateExp(rnd, x1, x2, alpha = None) : 
  """
    Exponential random distribution with constant "alpha", 
    limited to the range x1 to x2
  """
  if isinstance(x1, float) : x1 = Const(x1)
  if isinstance(x2, float) : x2 = Const(x2)
  if alpha is None or alpha == 0 : 
    return UniformRandom(rnd, x1, x2)
  else : 
    if isinstance(alpha, float) : alpha = Const(alpha)
    xi1 = Exp(-x1/alpha)
    xi2 = Exp(-x2/alpha)
    ximin = Min(xi1, xi2)
    ximax = Max(xi1, xi2)
    return Abs(alpha*Log(UniformRandom(rnd, ximin, ximax) ) )

def GeneratePt(rnd, mean, cut1, cut2) : 
  """
    Generate Pt distribution, with mean "mean", and miminum Pt "cut"
  """
  return GenerateExp(rnd, cut1, cut2, mean)

def GenerateEta(rnd) : 
  """
    Generate pseudorapidity, uniform from 2 to 5.
  """
  return UniformRandom(rnd, 2., 5.)    # Eta, uniform in (2., 5.)

def GeneratePhi(rnd) : 
  """
    Generate phi, uniform in 0, 2pi
  """
  return UniformRandom(rnd, 0., 2.*math.pi) # Phi, uniform in (0, 2pi)

def GenerateRotationAndBoost(moms, minit, meanpt, ptcut, rnd) : 
  """
    Generate 4-momenta of final state products according to 3-body phase space distribution
      moms   - initial particle momenta (in the rest frame)
      meanpt - mean Pt of the initial particle
      ptcut  - miminum Pt of the initial particle
      rnd    - Auxiliary random tensor
  """

  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, 200.)  # Pt in GeV
  eta = GenerateEta(rnd[:,1])          # Eta
  phi = GeneratePhi(rnd[:,2])          # Phi

  theta = 2.*Atan(Exp(-eta))     # Theta angle
  p  = pt/Sin(theta)             # Full momentum
  e  = Sqrt(p**2 + minit**2)     # Energy 

  px = p*Sin(theta)*Sin(phi)     # 3-momentum of initial particle
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)

  p4 = LorentzVector(Vector(px, py, pz), e)  # 4-momentum of initial particle

  rotphi   = UniformRandom(rnd[:,3], 0., 2*Pi())
  rotpsi   = UniformRandom(rnd[:,4], 0., 2*Pi())
  rottheta = Acos(UniformRandom(rnd[:,5], -1, 1.))

  moms2 = []
  for m in moms : 
    m1 = RotateLorentzVector(m, rotphi, rottheta, rotpsi)
    moms2 += [ BoostFromRest(m1, p4) ]

  return moms2

# TF tree to generate random cuts, generate final state momenta from the Dalitz plot sample, and run 
# the generated events through selection. 
def Selection(sample, rnd) : 

    # Trees to generate random cuts
    bc_ptcut   = GenerateExp(rnd[:,0], bounds['bcptcut'][0][0],  bounds['bcptcut'][0][1],  bounds['bcptcut'][1] )
    b_ptcut    = GenerateExp(rnd[:,1], bounds['bptcut'][0][0],   bounds['bptcut'][0][1],   bounds['bptcut'][1]  )
    k_ptcut    = GenerateExp(rnd[:,2], bounds['kptcut'][0][0],   bounds['kptcut'][0][1],   bounds['kptcut'][1]  )
    pi_ptcut   = GenerateExp(rnd[:,3], bounds['piptcut'][0][0],  bounds['piptcut'][0][1],  bounds['piptcut'][1] )
    bpi_ptcut  = GenerateExp(rnd[:,4], bounds['bpiptcut'][0][0], bounds['bpiptcut'][0][1], bounds['bpiptcut'][1])
    bk_ptcut   = GenerateExp(rnd[:,5], bounds['bkptcut'][0][0],  bounds['bkptcut'][0][1],  bounds['bkptcut'][1] )
    kpi_ptcut  = GenerateExp(rnd[:,6], bounds['kpiptcut'][0][0], bounds['kpiptcut'][0][1], bounds['kpiptcut'][1])

    # Tree to generate random momenta for a given Dalitz plot sample
    mom = observablesPhaseSpace.FinalStateMomenta( 
             observablesPhaseSpace.M2ab(sample), 
             observablesPhaseSpace.M2bc(sample) 
          )  # Genrate momenta in a frame of the decaying particle

    mom = GenerateRotationAndBoost(mom, md, meanpt, bc_ptcut, rnd[:,7:]) # Rotate and boost to the lab frame

    # Apply cuts
    sel = tf.greater(Pt(mom[0]), b_ptcut)
    sel = tf.logical_and(sel, tf.greater(Pt(mom[1]), k_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[2]), pi_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[0]+mom[1]+mom[2]), bc_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[0]+mom[1]), bk_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[0]+mom[2]), bpi_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[1]+mom[2]), kpi_ptcut))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[0]), etamin))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[1]), etamin))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[2]), etamin))
    sel = tf.logical_and(sel, tf.less(Eta(mom[0]), etamax))
    sel = tf.logical_and(sel, tf.less(Eta(mom[1]), etamax))
    sel = tf.logical_and(sel, tf.less(Eta(mom[2]), etamax))

    m2ab = Mass(mom[0]+mom[1])**2
    m2bc = Mass(mom[1]+mom[2])**2

    arrays = [] 
    for i in [ m2ab, m2bc, b_ptcut, k_ptcut, pi_ptcut, bk_ptcut, bpi_ptcut, kpi_ptcut, bc_ptcut ] : 
      arrays += [ tf.boolean_mask(i, sel) ]
    return arrays
