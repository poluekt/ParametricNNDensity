import math, sys

sys.path.append("../")

from TensorFlowAnalysis import *

md  = 5.280
mpi = 0.139
mk  = 0.498

etamin = 2. 
etamax = 5.

dlzPhsp = DalitzPhaseSpace( mk, mpi, mpi, md )
mPhsp = RectangularPhaseSpace( ((4., 8.), ) )
observablesPhaseSpace = CombinedPhaseSpace(dlzPhsp, mPhsp)

expPhaseSpace = VetoPhaseSpace(observablesPhaseSpace, 2, (5.0, 5.5) )

finalStateMultiplicity = 3

observablesTitles = [ "m^{2}(K#pi) (GeV^{2})", "m^{2}(#pi#pi) (GeV^{2})", "m(B) (GeV)" ]

observablesData = [ "m2kpi", "m2pipi", "mb" ]

observablesToys = [ "m2kpi", "m2pipi", "mb" ]

parametersList = [
    ("kmeanpt",  "Mean p_{T}(K) (GeV)",   (0.2, 1.),  0.6), 
    ("pimeanpt", "Mean p_{T}(#pi) (GeV)", (0.2, 1.),  0.30), 
    ("ptcut",    "Threshold p_{T} (GeV)", (0.1, 0.5), 0.25), 
    ("pcut",     "Threshold p (GeV)",     (1.0, 4.0), -6.), 
]

bounds = { i[0] : (i[2], i[3]) for i in parametersList }

def UniformRandom(rnd, x1, x2) : 
  """
    Uniform random numbers from x1 to x2
  """
#  print x1, x2
  return x1 + rnd*(x2-x1)

def GenerateExp(rnd, x1, x2, alpha = None) : 
  """
    Exponential random distribution with constant "alpha", 
    limited to the range x1 to x2
  """
  if isinstance(x1, float) : x1 = Const(x1)
  if isinstance(x2, float) : x2 = Const(x2)
  if alpha is None or alpha == 0 : 
    return UniformRandom(rnd, x1, x2)
  else : 
    if isinstance(alpha, float) : alpha = Const(alpha)
    xi1 = Exp(-x1/alpha)
    xi2 = Exp(-x2/alpha)
    ximin = Min(xi1, xi2)
    ximax = Max(xi1, xi2)
    return Abs(alpha*Log(UniformRandom(rnd, ximin, ximax) ) )

def GeneratePt(rnd, mean, cut1, cut2) : 
  """
    Generate Pt distribution, with mean "mean", and miminum Pt "cut"
  """
  return GenerateExp(rnd, cut1, cut2, mean)

def GenerateEta(rnd) : 
  """
    Generate pseudorapidity, uniform from 2 to 5.
  """
  return UniformRandom(rnd, 2., 5.)    # Eta, uniform in (2., 5.)

def GeneratePhi(rnd) : 
  """
    Generate phi, uniform in 0, 2pi
  """
  return UniformRandom(rnd, 0., 2.*math.pi) # Phi, uniform in (0, 2pi)

def MomentumResolution(p) :
  """
    Relative momentum resolution as a function of momentum (here = 0.5%, constant)
  """
  return 0.005

def MomentumScale(dm, moms) :
  """
    Function to calculate the momentum scale vactor for kinematic fit
      dm   : invariant mass shift from the desired value
      moms : list of 4-momenta of the final state particles
  """
  psum = sum(moms)  # sum of 4-momenta
  pvecsum = SpatialComponents(psum)
  esum = TimeComponent(psum)
  dedd = Const(0.)
  pdpdd = Const(0.)
  for mom in moms : 
    pm   = P(mom)  # Absolute momentum
    em   = TimeComponent(mom) # Energy
    pvec = SpatialComponents(mom) # 3-momentum
    s    = MomentumResolution(pm)
    dedd += s*pm**2/em
    pdpdd += ScalarProduct(pvecsum, pvec)*s
  return -dm/(2.*esum*dedd - 2.*pdpdd)

def KinematicFit(mfit, moms) : 
  """
    Kinematic fit to a fixed invariant mass for a multibody decay. 
    Returns the fitted mass and the list of final state 4-momenta. 
  """
  mcorr = Mass(sum(moms))
  for l in range(3) :
    dm2 = mcorr**2-mfit**2
    delta = MomentumScale(dm2, moms)
    moms2 = []
    for mom in moms :
      m2 = Mass(mom)**2
      momvec = SpatialComponents(mom)*Scalar(1+delta*MomentumResolution(P(mom)))
      mom2 = LorentzVector(
        momvec, 
        Sqrt(m2 + Norm(momvec)**2)
      )
      moms2 += [ mom2 ]
    moms = moms2
    mcorr = Mass(sum(moms))
  return mcorr, moms

def Generate4Momenta(rnd, meanpt, ptcut, m) : 
  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, Const(200.))  # Pt in GeV
  eta = GenerateEta(rnd[:,1])         # Eta
  phi = GeneratePhi(rnd[:,2])         # Phi

  theta = 2.*Atan(Exp(-eta))
  p  = pt/Sin(theta)     # Full momentum
  e  = Sqrt(p**2 + m**2) # Energy
  px = p*Sin(theta)*Sin(phi)
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)
  return LorentzVector(Vector(px, py, pz), e)

def GenerateCandidates(cuts, rnd, constant_cuts = False) : 
  meankpt  = cuts[0]
  meanpipt = cuts[1]
  ptcut    = cuts[2]
  pcut     = cuts[3]

  p4pi1 = Generate4Momenta(rnd[:,0:3], meanpipt, ptcut, Const(mpi) )
  p4pi2 = Generate4Momenta(rnd[:,3:6], meanpipt, ptcut, Const(mpi) )
  p4k   = Generate4Momenta(rnd[:,6:9], meankpt,  ptcut, Const(mk) )

  mb = Mass(p4k + p4pi1 + p4pi2)
  mfit, moms = KinematicFit(Const(md), [ p4k, p4pi1, p4pi2 ] )

  sel = tf.greater( P(moms[0]), pcut )
  sel = tf.logical_and(sel, tf.greater( P(moms[1]), pcut ) )
  sel = tf.logical_and(sel, tf.greater( P(moms[2]), pcut ) )

  m2kpi  = Mass(moms[0] + moms[1])**2
  m2pipi = Mass(moms[1] + moms[2])**2

  sel = tf.logical_and(sel, observablesPhaseSpace.Inside( tf.stack( [ m2kpi, m2pipi, mb ], axis = 1) ) )

  arrays = []
  outlist = [ m2kpi, m2pipi, mb ]
  if not constant_cuts : outlist += [ meankpt, meanpipt, ptcut, pcut ]
  for i in outlist : 
    arrays += [ tf.boolean_mask(i, sel) ]

  return arrays

def GenerateCandidatesAndCuts(rnd) : 

  cuts = []
  for i in range(len(bounds)) : 
    par = parametersList[i][0]
    alpha = parametersList[i][3]
    cuts += [ GenerateExp(rnd[:,i], bounds[par][0][0], bounds[par][0][1], alpha) ]

  return GenerateCandidates(cuts, rnd[:,len(bounds):])
