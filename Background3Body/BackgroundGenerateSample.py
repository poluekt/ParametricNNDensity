from ROOT import TRandom3, TMath, TGenPhaseSpace, TLorentzVector, TVector3, TFile, TNtuple
import array
import sys
import math

def GeneratePt(rnd, mean) : 
  pt = -mean*TMath.Log(rnd.Rndm())
  return pt

def GenerateEta(rnd) : 
  eta = 2. + 3.*rnd.Rndm()      # Eta, uniform in (2., 5.)
  return eta

def GeneratePhi(rnd) : 
  phi = 2.*TMath.Pi()*rnd.Rndm() # Phi, uniform in (0, 2pi)
  return phi

def MomentumResolution(p) :
  return 0.005

def MomentumScale(dm, mom) :
  p1m = mom[0].P()
  p2m = mom[1].P()
  p3m = mom[2].P()
  e1m = mom[0].E()
  e2m = mom[1].E()
  e3m = mom[2].E()
  p1 = mom[0].Vect()
  p2 = mom[1].Vect()
  p3 = mom[2].Vect()
  s1 = MomentumResolution(p1m)
  s2 = MomentumResolution(p2m)
  s3 = MomentumResolution(p3m)
  p = mom[0]+mom[1]+mom[2]
  pv = p.Vect()
  pm = p.P()
  e = p.E()
  dedd = s1*p1m**2/e1m + s2*p2m**2/e2m + s3*p3m**2/e3m
  pdpdd = pv.Dot(p1)*s1 + pv.Dot(p2)*s2 + pv.Dot(p3)*s3
  return -dm/(2.*e*dedd - 2.*pdpdd)

def KinematicFit(mfit, mom) : 
  mcorr = (mom[0]+mom[1]+mom[2]).M()
  for l in range(8) : 
    dm2 = mcorr**2-mfit**2
    delta = MomentumScale(dm2, mom)
    for k in range(3) : 
      m2 = mom[k].M2()
      mom[k] *= (1+delta*MomentumResolution(mom[k].P()))
      mom[k].SetE(TMath.Sqrt(m2 + mom[k].Vect().Mag2()))
    mcorr = (mom[0]+mom[1]+mom[2]).M()
  return mcorr

def Sqrt(x) : return math.sqrt(x)

def CosHelicityAngleDalitz(m2ab, m2bc, md, ma, mb, mc) :
  """
  Calculate cos(helicity angle) for set of two Dalitz plot variables
    m2ab, m2bc : Dalitz plot variables (inv. masses squared of AB and BC combinations)
    md : mass of the decaying particle
    ma, mb, mc : masses of final state particles
  """
  md2 = md**2
  ma2 = ma**2
  mb2 = mb**2
  mc2 = mc**2
  m2ac = md2 + ma2 + mb2 + mc2 - m2ab - m2bc
  mab = Sqrt(m2ab)
  mac = Sqrt(m2ac)
  mbc = Sqrt(m2bc)
  p2a = 0.25/md2*(md2-(mbc+ma)**2)*(md2-(mbc-ma)**2)
  p2b = 0.25/md2*(md2-(mac+mb)**2)*(md2-(mac-mb)**2)
  p2c = 0.25/md2*(md2-(mab+mc)**2)*(md2-(mab-mc)**2)
  eb = (m2ab-ma2+mb2)/2./mab
  ec = (md2-m2ab-mc2)/2./mab
  pb = Sqrt(eb**2-mb2)
  pc = Sqrt(ec**2-mc2)
  e2sum = (eb+ec)**2
  m2bc_max = e2sum-(pb-pc)**2
  m2bc_min = e2sum-(pb+pc)**2
  return (m2bc_max + m2bc_min - 2.*m2bc)/(m2bc_max-m2bc_min)

def SqDalitz(m2ab, m2bc, md, ma, mb, mc) : 
  cth = CosHelicityAngleDalitz(m2ab, m2bc, md, ma, mb, mc)
  minab = ma + mb
  maxab = md - mc
  th  = math.acos(cth)/math.pi
  mab = Sqrt(m2ab)
  mpr = math.acos(2*(mab - minab)/(maxab - minab) - 1.)/math.pi
  return (mpr, th)

def GenerateSingleCandidate(rnd, var) : 

  ptcut  = var['pt_cut']
  pcut   = var['p_cut']
  etamin = 2.
  etamax = 5.

  particles = (
    ("ks" , 0.498, var['ks_mean_pt'] ), 
    ('pip', 0.139, var['pi_mean_pt'] ), 
    ('pim', 0.139, var['pi_mean_pt'] )
  )

  mom = []

  for p in particles : 

    name   = p[0]
    m      = p[1]
    meanpt = p[2]

    sel = 0
    px = 0.
    py = 0. 
    pz = 0.
    e = 0.
    while not sel : 

      pt  = GeneratePt(rnd, meanpt)  # Pt in GeV
      eta = GenerateEta(rnd)         # Eta
      phi = GeneratePhi(rnd)         # Phi

      theta = 2.*TMath.ATan(TMath.Exp(-eta))
      p  = pt/TMath.Sin(theta)     # Full momentum
      e  = TMath.Sqrt(p**2 + m**2) # Energy

      px = p*TMath.Sin(theta)*TMath.Sin(phi)
      py = p*TMath.Sin(theta)*TMath.Cos(phi)
      pz = p*TMath.Cos(theta)
      gamma = TMath.Sqrt(1 + p**2/m**2)

      sel = 1
      if pt < ptcut   : sel = 0
      if p  < pcut    : sel = 0
      if eta < etamin : sel = 0
      if eta > etamax : sel = 0

    p4 = TLorentzVector(px, py, pz, e)
    mom += [ p4 ]
  
  return mom

def main() : 
  rnd = TRandom3()

  nev = 100000
  outfile = "sample.root"

  mfit = 5.2

  if len(sys.argv)>1 : outfile = sys.argv[1]

  f = TFile.Open(outfile, "RECREATE")
  variables = 'm:msq12:msq13:msq23:msqcorr12:msqcorr13:msqcorr23:mcorr:mpr:th'
  nt = TNtuple('nt', ' ', variables)

  ks_mean_pt = 0.3
  pi_mean_pt = 0.6
  p_cut  = 2.0
  pt_cut = 0.3

  var = {
      'ks_mean_pt' : ks_mean_pt, 
      'pi_mean_pt' : pi_mean_pt, 
      'p_cut' :      p_cut, 
      'pt_cut' :     pt_cut, 
  }

  j = 0
  for i in range(nev) : 
    mom = GenerateSingleCandidate(rnd, var)

    m = (mom[0]+mom[1]+mom[2]).M()

    if m < 4. or m > 8. : continue

    msq12 = (mom[0]+mom[1]).M()**2
    msq13 = (mom[0]+mom[2]).M()**2
    msq23 = (mom[1]+mom[2]).M()**2

    mcorr = KinematicFit(mfit, mom)
    if abs(mcorr-mfit)>0.1 : continue

    msqcorr12 = (mom[0]+mom[1]).M()**2
    msqcorr13 = (mom[0]+mom[2]).M()**2
    msqcorr23 = (mom[1]+mom[2]).M()**2
    mcorr = (mom[0]+mom[1]+mom[2]).M()

    (mpr, th) = SqDalitz(msqcorr12, msqcorr23, mfit, 0.498, 0.139, 0.139)

    if j % 1000 == 0 : 
      print i, j, nev, m, mcorr, mpr, th
    j += 1

    nt.Fill(m, msq12, msq13, msq23, msqcorr12, msqcorr13, msqcorr23, mcorr, mpr, th)

  nt.Write()
  f.Close()

if __name__ == "__main__" : 
  main()

