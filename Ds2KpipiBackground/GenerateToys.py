import array
import sys
import math
import numpy as np
from root_numpy import array2root

sys.path.append("../")

from TensorFlowAnalysis import *
from DistributionModel import parametersList, generatedVariables, GenerateCandidatesAndCuts, randomArraySize

import sys, os

#os.environ["CUDA_VISIBLE_DEVICES"] = ""   # Do not use GPU

def main() : 

  nev = 500000
  outfile = "toy_tuple.root"

  chunk_size = 1000000  # Events will be generated in chunks of this size

  SetDoublePrecision()

  bounds = { i[0] : (i[2], i[3]) for i in parametersList }  # Bounds and exponential factor for generation of cuts

  struct  = [ (name, float) for name in generatedVariables ]
  struct += [ (i[0], float) for i in parametersList ]  # Structure of the output ntuple

  init = tf.global_variables_initializer() # Initialise TensorFlow
  sess = tf.Session()
  sess.run(init)

  n = 0   # Current tuple size

  # Placeholders for auxiliary random array and Dalitz plot sample
  rnd_ph    = tf.placeholder(FPType(), shape = (None, None), name = "rnd")

  while(True) : 

    # Auxiliaru random array
    rnd = np.stack([ Random(chunk_size) for i in range(randomArraySize + len(bounds)) ], axis = 1) 
    print rnd.shape
    arrays = sess.run( GenerateCandidatesAndCuts( rnd_ph ), feed_dict = { rnd_ph : rnd } )

    # Store everything into ROOT file
    recarray = np.rec.fromarrays( arrays , dtype = struct)
    if n == 0 : mode = "recreate"
    else :      mode = "update"
    array2root(recarray, outfile, mode = mode)

    # Increment counters and check if we are done
    size = len(recarray)
    n += size
    if n > nev : break
    print "Selected size = ", n, " last = ", size

if __name__ == "__main__" : 
  main()
