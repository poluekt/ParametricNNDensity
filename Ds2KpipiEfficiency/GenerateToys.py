import array
import sys
import math
import numpy as np
from root_numpy import array2root

sys.path.append("../")

from TensorFlowAnalysis import *
from DistributionModel import parametersList, observablesToys, observablesPhaseSpace, SelectionWithRandomCuts, randomArraySize

import sys, os

os.environ["CUDA_VISIBLE_DEVICES"] = ""   # Do not use GPU

def main() : 

  nev = 500000
  outfile = "toy_tuple.root"

  chunk_size = 1000000  # Events will be generated in chunks of this size

  SetDoublePrecision()

  bounds = { i[0] : (i[2], i[3]) for i in parametersList }  # Bounds and exponential factor for generation of cuts

  struct  = [ (name, float) for name in observablesToys ]
  struct += [ (i[0], float) for i in parametersList ]  # Structure of the output ntuple

  init = tf.global_variables_initializer() # Initialise TensorFlow
  sess = tf.Session()
  sess.run(init)

  n = 0   # Current tuple size

  # Placeholders for auxiliary random array and Dalitz plot sample
  rnd_ph    = tf.placeholder(FPType(), shape = (None, None), name = "rnd")
  sample_ph = tf.placeholder(FPType(), shape = (None, None), name = "sample")

  while(True) : 

    # Create Dalitz plot sample
    unfiltered_sample = observablesPhaseSpace.UnfilteredSample(chunk_size)  # Unfiltered array
    sample = sess.run( observablesPhaseSpace.Filter(sample_ph), 
                       feed_dict = { sample_ph : unfiltered_sample } )      # Filtered sample (inside Dalitz phase space)
    size = sample.shape[0]
    print "Filtered chunk size = ", size

    # Generate final state momenta from Dalitz plot and run through selection
    rnd = np.stack([ Random(size) for i in range(randomArraySize) ], axis = 1)    # Auxiliary random array
    arrays = sess.run( SelectionWithRandomCuts(sample_ph, rnd_ph),
                       feed_dict = { rnd_ph : rnd, sample_ph : sample }  )

    # Store everything into ROOT file
    recarray = np.rec.fromarrays( arrays , dtype = struct)
    if n == 0 : mode = "recreate"
    else :      mode = "update"
    array2root(recarray, outfile, mode = mode)

    # Increment counters and check if we are done
    size = len(recarray)
    n += size
    if n > nev : break
    print "Selected size = ", n, " last = ", size

if __name__ == "__main__" : 
  main()
