import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *

SetSinglePrecision()

from nn import estimate_density

#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from ROOT import gROOT, gStyle

from DistributionModel import observablesPhaseSpace, observablesToys, observablesTitles

gROOT.ProcessLine(".x ../lhcbstyle2.C")
gStyle.SetPalette(56)

variables = observablesToys
titles = observablesTitles

bounds = observablesPhaseSpace.Bounds()

phsp = observablesPhaseSpace

estimate_density(
  phsp = phsp, 
  variables = variables, 
  bounds = bounds, 
  titles = titles, 
  treename = "tree", 
  learning_rate = 0.001, 
  training_epochs = 20000, 
  print_step = 50, 
  display_step = 200, 
  weight_penalty = 0.5, 
  n_hidden  = [ 32, 64, 32, 8 ], 
  norm_size = 500000, 
  path = "./", 
  initfile = "init_2d.npy", 
  calibfile = "test_tuple.root", 
  outfile = "eff_train_2d", 
  selection = "", 
  seed = 2, 
)
