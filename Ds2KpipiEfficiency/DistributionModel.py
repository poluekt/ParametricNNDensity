import math, sys

sys.path.append("../")

from TensorFlowAnalysis import *

md  = 1.96834
mpi = 0.13957
mk  = 0.49368

etamin = 2. 
etamax = 5.
meanpt = 1.

#observablesPhaseSpace = DalitzPhaseSpace( mk, mpi, mpi, md )
#observablesTitles = [ "m^{2}(K^{+}#pi^{#font[122]{-}}) (GeV^{2})", "m^{2}(#pi^{+}#pi^{#font[122]{-}}) (GeV^{2})" ]
dalitzPhaseSpace = DalitzPhaseSpace( mk, mpi, mpi, md )
observablesPhaseSpace = RectangularPhaseSpace( ((0., 1.), (0., 1.)) )
observablesTitles = [ "m'", "#theta'" ]
observablesData = [ "mprime", "thetaprime" ]
observablesToys = [ "mprime", "thetaprime" ]

parametersList = [
  ("ptcut",     "Track p_{T} cut (GeV)",       (0.1, 1.), -1.0), 
  ("pcut",      "Track p cut (GeV)",           (1., 10.), -10.),
  ("dptcut",    "D^{+}_{s} p_{T} cut (GeV)",   (0.,  5.),  1.3), 
  ("maxptcut",  "max p_{T} cut (GeV)",         (0.5, 3.), -2.5), 
  ("sumptcut",  "sum p_{T} cut (GeV)",         (2.5, 6.), -2.5), 
]

#trueCuts = [ Const(0.4), Const(3.), Const(2.0), Const(1.5), Const(3.) ]
trueCuts = [ Const(0.4), Const(3.), Const(2.0), Const(1.), Const(3.) ]

randomArraySize = 11

bounds = { i[0] : (i[2], i[3]) for i in parametersList }

def UniformRandom(rnd, x1, x2) : 
  """
    Uniform random numbers from x1 to x2
  """
  return x1 + rnd*(x2-x1)

def GenerateExp(rnd, x1, x2, alpha = None) : 
  """
    Exponential random distribution with constant "alpha", 
    limited to the range x1 to x2
  """
  if isinstance(x1, float) : x1 = Const(x1)
  if isinstance(x2, float) : x2 = Const(x2)
  if alpha is None or alpha == 0 : 
    return UniformRandom(rnd, x1, x2)
  else : 
    if isinstance(alpha, float) : alpha = Const(alpha)
    xi1 = Exp(-x1/alpha)
    xi2 = Exp(-x2/alpha)
    ximin = Min(xi1, xi2)
    ximax = Max(xi1, xi2)
    return Abs(alpha*Log(UniformRandom(rnd, ximin, ximax) ) )

def GeneratePt(rnd, mean, cut1, cut2) : 
  """
    Generate Pt distribution, with mean "mean", and miminum Pt "cut"
  """
  return GenerateExp(rnd, cut1, cut2, mean)

def GenerateEta(rnd) : 
  """
    Generate pseudorapidity, uniform from 2 to 5.
  """
  return UniformRandom(rnd, 2., 5.)    # Eta, uniform in (2., 5.)

def GeneratePhi(rnd) : 
  """
    Generate phi, uniform in 0, 2pi
  """
  return UniformRandom(rnd, 0., 2.*math.pi) # Phi, uniform in (0, 2pi)

def GenerateRotationAndBoost(moms, minit, meanpt, ptcut, rnd) : 
  """
    Generate 4-momenta of final state products according to 3-body phase space distribution
      moms   - initial particle momenta (in the rest frame)
      meanpt - mean Pt of the initial particle
      ptcut  - miminum Pt of the initial particle
      rnd    - Auxiliary random tensor
  """

  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, 200.)  # Pt in GeV
  eta = GenerateEta(rnd[:,1])          # Eta
  phi = GeneratePhi(rnd[:,2])          # Phi

  theta = 2.*Atan(Exp(-eta))     # Theta angle
  p  = pt/Sin(theta)             # Full momentum
  e  = Sqrt(p**2 + minit**2)     # Energy 

  px = p*Sin(theta)*Sin(phi)     # 3-momentum of initial particle
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)

  p4 = LorentzVector(Vector(px, py, pz), e)  # 4-momentum of initial particle

  rotphi   = UniformRandom(rnd[:,3], 0., 2*Pi())
  rotpsi   = UniformRandom(rnd[:,4], 0., 2*Pi())
  rottheta = Acos(UniformRandom(rnd[:,5], -1, 1.))

  moms2 = []
  for m in moms : 
    m1 = RotateLorentzVector(m, rotphi, rottheta, rotpsi)
    moms2 += [ BoostFromRest(m1, p4) ]

  return moms2

def Selection(sample, cuts, rnd, constant_cuts = False) : 
    ptcut     = cuts[0]
    pcut      = cuts[1]
    d_ptcut   = cuts[2]
    max_ptcut = cuts[3]
    sum_ptcut = cuts[4]

    dalitzSample = dalitzPhaseSpace.FromSquareDalitzPlot(sample[:,0], sample[:,1])

    # Graph to generate random momenta for a given Dalitz plot sample
    mom = dalitzPhaseSpace.FinalStateMomenta( 
             dalitzPhaseSpace.M2ac(dalitzSample),   # Note AB<->AC because FroSquareDalitzPlot works with AC
             dalitzPhaseSpace.M2bc(dalitzSample) 
          )  # Genrate momenta in a frame of the decaying particle

    mom = GenerateRotationAndBoost(mom, md, meanpt, d_ptcut, rnd[:,:]) # Rotate and boost to the lab frame

    # Apply cuts
    sel = tf.greater(Pt(mom[0]), ptcut)
    sel = tf.logical_and(sel, tf.greater(Pt(mom[1]), ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[2]), ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[0]+mom[1]+mom[2]), d_ptcut))
    sel = tf.logical_and(sel, tf.greater(P(mom[0]), pcut))
    sel = tf.logical_and(sel, tf.greater(P(mom[1]), pcut))
    sel = tf.logical_and(sel, tf.greater(P(mom[2]), pcut))
    sel = tf.logical_and(sel, tf.greater(Max(Max(Pt(mom[0]), Pt(mom[1])), Pt(mom[2])), max_ptcut))
    sel = tf.logical_and(sel, tf.greater(Pt(mom[0])+Pt(mom[1])+Pt(mom[2]), sum_ptcut))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[0]), etamin))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[1]), etamin))
    sel = tf.logical_and(sel, tf.greater(Eta(mom[2]), etamin))
    sel = tf.logical_and(sel, tf.less(Eta(mom[0]), etamax))
    sel = tf.logical_and(sel, tf.less(Eta(mom[1]), etamax))
    sel = tf.logical_and(sel, tf.less(Eta(mom[2]), etamax))

    m2ab = Mass(mom[0]+mom[1])**2
    m2bc = Mass(mom[1]+mom[2])**2

    mprime = sample[:,0]
    thetaprime = sample[:,1]

    arrays = [] 
    outlist = [ mprime, thetaprime ]
    if not constant_cuts : outlist += [ ptcut, pcut, d_ptcut, max_ptcut, sum_ptcut ]
    for i in outlist : 
      arrays += [ tf.boolean_mask(i, sel) ]
    return arrays

# TF graph to generate random cuts, generate final state momenta from the Dalitz plot sample, and run 
# the generated events through selection. 
def SelectionWithRandomCuts(sample, rnd) : 

    # Graphs to generate random cuts
    ptcut      = GenerateExp(rnd[:,0], bounds['ptcut'][0][0],    bounds['ptcut'][0][1],    bounds['ptcut'][1]  )
    pcut       = GenerateExp(rnd[:,1], bounds['pcut'][0][0],     bounds['pcut'][0][1],     bounds['pcut'][1]  )
    d_ptcut    = GenerateExp(rnd[:,2], bounds['dptcut'][0][0],   bounds['dptcut'][0][1],   bounds['dptcut'][1]  )
    max_ptcut  = GenerateExp(rnd[:,3], bounds['maxptcut'][0][0], bounds['maxptcut'][0][1], bounds['maxptcut'][1] )
    sum_ptcut  = GenerateExp(rnd[:,4], bounds['sumptcut'][0][0], bounds['sumptcut'][0][1], bounds['sumptcut'][1] )

    cuts = [ ptcut, pcut, d_ptcut, max_ptcut, sum_ptcut ]

    return Selection(sample, cuts, rnd[:,5:])
