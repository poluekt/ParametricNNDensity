import tensorflow as tf
import sys, os

sys.path.append("../")

from TensorFlowAnalysis import *
import numpy as np
from root_numpy import root2array, rec2array
from rootpy.plotting import Hist2D, Hist1D

from DistributionModel import parametersList, observablesPhaseSpace, observablesData, observablesTitles
from nn import init_fixed_weights_biases, multilayer_perceptron, multidim_display
from ROOT import gROOT

os.environ["CUDA_VISIBLE_DEVICES"] = ""   # Do not use GPU

gROOT.ProcessLine(".x ../lhcbstyle.C")

print_step = 50        # print statistics every 50 epochs
norm_size = 1000000    # size of the normalisation sample (random uniform)
path = "./"
initfile = "train.npy"           # file with the trained parameters of the NN
calibfile = "D2hhhh/rec_5d.root" # Sample to fit
outfile = "result"               # Prefix for output files (text and pdf)
seed = 1                         # initial random seed

if len(sys.argv)>1 : calibfile = sys.argv[1]  # file to fit is the first command line parameter
if len(sys.argv)>2 : outfile = sys.argv[2]    # output prefix is the second parameter
if len(sys.argv)>3 : seed = int(sys.argv[3])  # optionally, initial seed

init_w = np.load(initfile)       # Load NN parameters 

# Load sample to fit
sample = root2array(path + calibfile, treename = "tree",
                    branches = observablesData,
                    selection = "")

# transform recarray into a simple numpy array for 5 variables of the phase space
data_sample = rec2array(sample, observablesData)

# TF placeholders for data and normalisation sample to be used for the likelihood graph
data_ph = observablesPhaseSpace.data_placeholder
norm_ph = observablesPhaseSpace.norm_placeholder

# Initialise NN weights and biases from the loaded file
(weights, biases) = init_fixed_weights_biases(init_w)

ndim = observablesPhaseSpace.Dimensionality()
observablesBounds = observablesPhaseSpace.Bounds()

# Density model as a multilayer perceptron
def model(x, pars) : 
  # Constant vectors of fit parameters (the same for every data point)
  vec = tf.reshape( tf.concat( [ tf.constant(ndim*[0.], dtype = FPType() ), pars ], axis = 0 ), [ 1, ndim + len(pars) ] )
  # Input tensor for MLP, 5 first variables are data, the rest are constant optimisable parameters 
  x2 = tf.pad( x, [[0, 0], [0, len(pars)]], 'CONSTANT') + vec
  return multilayer_perceptron(x2, weights, biases)

# Initialise random seeds
np.random.seed(seed)
tf.set_random_seed(seed)

# Declare fit parameters 
pars = [ FitParameter(par[0], (par[2][0]+par[2][1])/2., par[2][0], par[2][1]) for par in parametersList ]

# Create graphs for model over data and normalisation samples
data_model = model(data_ph, pars)
norm_model = model(norm_ph, pars)

# Unbinned negative log likelihood
nll = UnbinnedNLL( data_model, Integral( norm_model ) )

# Initializing the variables
init = tf.global_variables_initializer()

with tf.Session() as sess :
    sess.run(init)
 
    # Normalisation sample is a uniform random sample in 5D phase space
    norm_sample = sess.run( observablesPhaseSpace.UniformSample( norm_size ) )
#    norm_sample = sess.run( observablesPhaseSpace.RectangularGridSample( 201,201 ) )

    # Data sample, run through phase space filter just in case
    data_sample = sess.run( observablesPhaseSpace.Filter(data_sample) )

    # Initialise multidimensional density display object
    display = multidim_display(data_sample, observablesTitles, observablesBounds, bins2d = 20, bins1d = 50, fitbins2d = 20, fitbins1d = 50 )

    print "Normalisation sample size = ", len(norm_sample)
    print norm_sample
    print "Data sample size = ", len(data_sample)
    print data_sample

    # Run minimisation 10 times, choose the best NLL value
    best_nll = 1e10
    for i in range(0, 10) : 
      for p in pars : p.randomise(sess)
      result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample } )
      print result
      if result['loglh'] < best_nll :   # If we got the best NLL so far
        best_nll = result['loglh']
        norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )  # Calculate PDF
        display.draw(norm_sample, norm_pdf, outfile + ".pdf")              # Draw PDF
        WriteFitResults(result, outfile + ".txt")                          # Write fit result

    print("Optimization Finished!")

    # Plot the best fit result
    ReadFitResults(sess, outfile + ".txt")
    result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample } )
    norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )
    display.draw(norm_sample, norm_pdf, outfile + ".pdf")
