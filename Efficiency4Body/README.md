Scripts to parametrise efficiency of four-body D0->KKpipi decay
in the 5D phase space: 
  * M(KK)               - invariant mass of KK combination
  * M(pipi)             - invariant mass of pipi combination
  * cos theta_hel (K)   - cosine of helicity angle of a kaon 
  * cos theta_hel (pi)  - cosine of helicity angle of a pion 
  * phi                 - angle between the KK and pipi decay planes

What is parametrised is in fact the product of efficiency and phase space density 
(the latter is non-uniform in the variables defined above)

Content of the directory: 
  * D2hhhh/              - directory with input ntuples
  * D2hhhh/gen.root      - Generator-level MC sample
  * D2hhhh/rec.root      - Reconstructed full MC sample
  * D2hhhh/gen_5d.root   - Processed generator-level MC (created by ProcessMC.py script) 
                           with the 5 phase space variables
  * D2hhhh/rec_5d.root   - same for reconstructed full MC sample
  * batch.sh             - shell script to run parametrisation
  * GenerateToys.py      - Sctipt to generate D->KKpipi events with toy efficiency model 
                           and randomised effective parameters. 
  * TrainNN.py           - Script to train NN representation of the D->KKpipi toy efficiency
                           model. 
  * ProcessSample.py     - Script to process full MC samples and produce 5D phase space variables. 
  * FitSample.py         - Script to fit NN representation of the toy efficiency model to full MC 
                           data. 
  * DistributionModel.py - Data structures and functions which describe the model for 4-body efficiency. 
                           In principle, this should be the only file to touch to tweak the efficiency
                           model. Other python scripts are very generic. 

Prerequisites: 
  * ROOT with pyROOT interface
  * rootpy and root-numpy libraries
  * TensorFlow version v1r9 or _older_ (v1r10 and more recent do not work yet with TensorFlowAnalysis!)

Running the code: 
  $ source batch.sh

Tweaking the model 
  Edit the file DistributionModel.py
