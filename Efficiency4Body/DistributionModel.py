import math, sys

sys.path.append("../")

from TensorFlowAnalysis import *

# Masses of the particles
md = 1.865
mk = 0.498
mpi = 0.139

# Eta ranges for tracks
etamin = 2. 
etamax = 5.

# Mean pt for inital particle production (constant in this model). 
meanpt = 5.

# Phase space for kinematic observables in the sample
observablesPhaseSpace = FourBodyHelicityPhaseSpace(mk, mk, mpi, mpi, md)

# Titles of the observables for the plots
observablesTitles = [ "m(KK) (GeV)", "m(#pi#pi) (GeV)", "cos#theta_{KK}", "cos#theta_{#pi#pi}", "#phi" ]

# Variable names for the data/MC sample
observablesData = [ "mkk", "mpipi", "costhk", "costhpi", "phi" ]

# Variable names for toy MC Ntuple used in the training
observablesToys = [ "mkk", "mpipi", "costhk", "costhpi", "phi" ]

# List of tunable parameters of the model. 
# Each variable is a tuple in the form
#   (var_name, var_title, range, exp_factor), where
#      var_name:   Name of the variable for toy MC Ntuple / Minuit
#      var_title:  title used for the plots
#      range:      range of the allowed values in the form of a tuple (min, max)
#      exp_factor: Exponential slope factor (>0 for falling distribution, <0 for raising) 
#                  for random generation of the cut distribution, 
#                  tune it such that the final distribution of tunable parameters in the 
#                  toy MC Ntuple is approximately flat (or, at least, not too peaking), 
#                  which improves training. Use None for flat distribution. 
parametersList = [
    ("dptcut",    "Threshold p_{T}(D) (GeV)",     (1., 5.) ,   3.), 
    ("ptcut",     "Threshold p_{T}(track) (GeV)", (0.1, 1.),   None), 
    ("pcut",      "Threshold p(track) (GeV)",     (1., 10.),   None), 
    ("maxptcut",  "Threshold max p_{T} (GeV)",    (0.5, 3.),   None), 
    ("sumptcut",  "Threshold #Sum p_{T} (GeV)",   (2.5, 6.),   -1.), 
]

# Maximum density of phase space term for accept-reject method used at the stage of toy MC generation
maximumDensity = 0.015  

# dictionary of parameter bounds
bounds = { i[0] : (i[2], i[3]) for i in parametersList }

def UniformRandom(rnd, x1, x2) : 
  """
    Uniform random numbers from x1 to x2
  """
  return x1 + rnd*(x2-x1)

def GenerateExp(rnd, x1, x2, alpha = None) : 
  """
    Exponential random distribution with constant "alpha", 
    limited to the range x1 to x2
  """
  if isinstance(x1, float) : x1 = Const(x1)
  if isinstance(x2, float) : x2 = Const(x2)
  if alpha is None or alpha == 0 : 
    return UniformRandom(rnd, x1, x2)
  else : 
    if isinstance(alpha, float) : alpha = Const(alpha)
    xi1 = Exp(-x1/alpha)
    xi2 = Exp(-x2/alpha)
    ximin = Min(xi1, xi2)
    ximax = Max(xi1, xi2)
    return Abs(alpha*Log(UniformRandom(rnd, ximin, ximax) ) )

def GeneratePt(rnd, mean, cut1, cut2) : 
  """
    Generate Pt distribution, with mean "mean", and miminum Pt "cut"
  """
  return GenerateExp(rnd, cut1, cut2, mean)

def GenerateEta(rnd) : 
  """
    Generate pseudorapidity, uniform from 2 to 5.
  """
  return UniformRandom(rnd, etamin, etamax)

def GeneratePhi(rnd) : 
  """
    Generate phi, uniform in 0, 2pi
  """
  return UniformRandom(rnd, 0., 2.*math.pi)

def GenerateRotationAndBoost(moms, minit, meanpt, ptcut, rnd) : 
  """
    Generate 4-momenta of final state products according to phase space distribution
      moms   - list of initial particle momenta (in the rest frame) tensors
      minit  - mass of the initial particle
      meanpt - mean Pt of the initial particle
      ptcut  - miminum Pt of the initial particle
      rnd    - Auxiliary random tensor with 6 rows (rnd.shape[1] == 6)
  """

  pt  = GeneratePt(rnd[:,0], meanpt, ptcut, 200.)  # Pt in GeV
  eta = GenerateEta(rnd[:,1])          # Eta
  phi = GeneratePhi(rnd[:,2])          # Phi

  theta = 2.*Atan(Exp(-eta))     # Theta angle
  p  = pt/Sin(theta)             # Full momentum
  e  = Sqrt(p**2 + minit**2)     # Energy 

  px = p*Sin(theta)*Sin(phi)     # 3-momentum of initial particle
  py = p*Sin(theta)*Cos(phi)
  pz = p*Cos(theta)

  p4 = LorentzVector(Vector(px, py, pz), e)  # 4-momentum of initial particle

  rotphi   = UniformRandom(rnd[:,3], 0., 2*Pi())
  rotpsi   = UniformRandom(rnd[:,4], 0., 2*Pi())
  rottheta = Acos(UniformRandom(rnd[:,5], -1, 1.))

  moms2 = []
  for m in moms : 
    m1 = RotateLorentzVector(m, rotphi, rottheta, rotpsi)
    moms2 += [ BoostFromRest(m1, p4) ]

  return moms2

def Selection(sample, rnd) : 
  """
    TF tree to generate random cuts, generate final state momenta from the Dalitz plot sample, and run 
    the generated events through selection. 
  """

  # Trees to generate random cuts
  d_ptcut     = GenerateExp(rnd[:,0], bounds['dptcut'][0][0],   bounds['dptcut'][0][1],   bounds['dptcut'][1]   )
  ptcut       = GenerateExp(rnd[:,1], bounds['ptcut'][0][0],    bounds['ptcut'][0][1],    bounds['ptcut'][1]   )
  pcut        = GenerateExp(rnd[:,2], bounds['pcut'][0][0],     bounds['pcut'][0][1],     bounds['pcut'][1]  )
  max_ptcut   = GenerateExp(rnd[:,3], bounds['maxptcut'][0][0], bounds['maxptcut'][0][1], bounds['maxptcut'][1] )
  sum_ptcut   = GenerateExp(rnd[:,4], bounds['sumptcut'][0][0], bounds['sumptcut'][0][1], bounds['sumptcut'][1] )

  # Tree to generate random momenta for a given Dalitz plot sample
  mom = observablesPhaseSpace.FinalStateMomenta( sample )  # Genrate momenta in a frame of the decaying particle
  mom = GenerateRotationAndBoost(mom, md, meanpt, d_ptcut, rnd[:,4:]) # Rotate and boost to the lab frame

  # Create selection mask (tensor of boolean values) to select events satisfying the cuts
  sel = tf.greater(Pt(mom[0]+mom[1]+mom[2]+mom[3]), d_ptcut)
  sel = tf.logical_and(sel, tf.greater(Pt(mom[0]), ptcut))
  sel = tf.logical_and(sel, tf.greater(Pt(mom[1]), ptcut))
  sel = tf.logical_and(sel, tf.greater(Pt(mom[2]), ptcut))
  sel = tf.logical_and(sel, tf.greater(Pt(mom[3]), ptcut))
  sel = tf.logical_and(sel, tf.greater(P(mom[0]), pcut))
  sel = tf.logical_and(sel, tf.greater(P(mom[1]), pcut))
  sel = tf.logical_and(sel, tf.greater(P(mom[2]), pcut))
  sel = tf.logical_and(sel, tf.greater(P(mom[3]), pcut))
  sel = tf.logical_and(sel, tf.greater(Max(Max(Pt(mom[0]), Pt(mom[1])), Max(Pt(mom[2]), Pt(mom[3]))), max_ptcut))
  sel = tf.logical_and(sel, tf.greater(Pt(mom[0])+Pt(mom[1])+Pt(mom[2])+Pt(mom[3]), sum_ptcut))
  sel = tf.logical_and(sel, tf.greater(Eta(mom[0]), etamin))
  sel = tf.logical_and(sel, tf.greater(Eta(mom[1]), etamin))
  sel = tf.logical_and(sel, tf.greater(Eta(mom[2]), etamin))
  sel = tf.logical_and(sel, tf.greater(Eta(mom[3]), etamin))
  sel = tf.logical_and(sel, tf.less(Eta(mom[0]), etamax))
  sel = tf.logical_and(sel, tf.less(Eta(mom[1]), etamax))
  sel = tf.logical_and(sel, tf.less(Eta(mom[2]), etamax))
  sel = tf.logical_and(sel, tf.less(Eta(mom[3]), etamax))

  ma1a2 = observablesPhaseSpace.Ma1a2(sample)
  mb1b2 = observablesPhaseSpace.Mb1b2(sample)
  coshela = observablesPhaseSpace.CosHelicityA(sample)
  coshelb = observablesPhaseSpace.CosHelicityB(sample)
  phi = observablesPhaseSpace.Phi(sample)
#  dens = observablesPhaseSpace.Density(sample)

  arrays = [] 
  for i in [ ma1a2, mb1b2, coshela, coshelb, phi, d_ptcut, ptcut, pcut, max_ptcut, sum_ptcut ] : 
    arrays += [ tf.boolean_mask(i, sel) ]
  return arrays

def PhaseSpaceFilter(sample) : 
  """
    Tree to select only the events within the phase space, distributed according to phase space density. 
    Unless it's the 2D Dalitz plot when the density is constant, it should run accept-reject toy MC 
    to shape the density. 
  """
  return AcceptRejectSample( observablesPhaseSpace.Density, observablesPhaseSpace.Filter(sample) )
