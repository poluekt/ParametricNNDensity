# Generate toy sample with random cuts for NN training
python GenerateToys.py

# Train NN on this sample
python TrainNN.py

# Process full MC samples to produce sample in 5D decay phase space
python ProcessSample.py gen
python ProcessSample.py rec

# Fit the full MC sample using NN parametrization 
python FitSample.py D2hhhh/gen_5d.root gen_result 1
python FitSample.py D2hhhh/rec_5d.root rec_result 1
